package lab6;
import org.junit.Test;
import static org.junit.Assert.*;
public class ConeTest {
    @Test
    public void TestingConstructorConeInvalidHeight(){
        try{
        Cone cone = new Cone(2,0);
        fail("Exception did not suceeed");

        }
        catch(IllegalArgumentException e){
        }
    }
    @Test
    public void TestingConstructorConeInvalidRadius(){
        try{
        Cone cone = new Cone(0,2);
        fail("Exception did not suceeed");
        }
        catch(IllegalArgumentException e){

        }
    }
    @Test
    public void TestingGetVolumeCone(){
        Cone cone = new Cone(2,2);
        assertEquals(8.377580409572781,cone.getVolume(),0.0001);
    }
    @Test 
    public void TestingGetSurfaceAreaCone(){
        Cone cone = new Cone(2,2);
        assertEquals(33.132741228718345,cone.getSurfaceArea(),0.0001);
    }
    @Test
    public void testingGetterRadius(){
        Cone cone = new Cone(5,6);
        assertEquals(5,cone.getRadius(),0.001);
    }
    @Test
    public void testingGetterHeight(){
        Cone cone = new Cone(5,6);
        assertEquals(6,cone.getHeight(),0.001);
    }
}
