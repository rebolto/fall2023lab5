package lab6;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class SphereTest {
    @Test
    public void testConstrustorSphere() {
        try {
            
            Sphere s = new Sphere(0);
            
        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public void testSphereVolume() {
        try {

            Sphere s = new Sphere(2);
            assertEquals(33.51032, s.getVolume(), 0.0001);
        } catch (IllegalArgumentException e) {
            fail();
        }
    }

    @Test
    public void testSphereSurface(){
        try{
            Sphere s = new Sphere(1);
            assertEquals(12.56637, s.getSurfaceArea(), 0.0001);
        } catch (IllegalArgumentException e){
            fail();
        }
    }
    @Test
    public void testGetRadius(){
            Sphere s = new Sphere(2);
            assertEquals(2,s.getRadius() , 0.0001);
        
    }
}
