package lab6;
/*
 * @author Noah Diplarakis
 * @version 10/4/2023
 */
public class Sphere implements Shape3d {
    private double radius;
    public Sphere(double radius){
        if(radius <= 0){
          throw new IllegalArgumentException("Radius is a invalid value");
      }
        this.radius = radius;
    }
/*
 * @return  surface area
 */
  public double getSurfaceArea(){
      if(this.radius <= 0){
          throw new IllegalArgumentException("Radius is a invalid value");
      }
      else 
      return (4 * Math.PI *Math.pow(this.radius,2));


    }
/*
 * @return the volume
 */
  public double getVolume(){
      if(this.radius <= 0){
          throw new IllegalArgumentException("Radius is a invalid value");
      }
      else 
      return ((4.0/3.0) * Math.PI * Math.pow(this.radius,3));}

    
/*
 * @return the radius
 */
  public double getRadius(){
      return this.radius;
    }
}