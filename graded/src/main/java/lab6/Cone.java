/**
 * @author Danny Hoang
 * @version 10/2/2023
 */
package lab6;
import java.lang.Math;
public class Cone implements Shape3d {

    private double radius;
    private double height;

    public Cone(double radius, double height){
        

        if(height <= 0 || radius <= 0){
            throw new IllegalArgumentException("height or radius is invalid");
        }
        else{
        this.height = height;
        this.radius = radius;
        }
    }
    /**
    * @return the volume area of a CONE
    */
    public double getVolume() {
        if(this.height < 0 || this.radius < 0  ){
            throw new IllegalArgumentException("invalid numbers");
        }
        else
        return (Math.PI * (Math.pow(this.radius, 2)) * (this.height / 3.0));
    }

    /**
    * @return the surface area of a CONE
    */
    public double getSurfaceArea() {
        if(this.height < 0 || this.radius < 0){
             throw new IllegalArgumentException("invalid numbers");
        }
        else
        return (2.0 * Math.pow(this.radius,2) + 2.0 * Math.PI * this.radius * this.height); 
    }
    /**
    * @return the radius of the object
    */
    public double getRadius(){
        return this.radius;
    }
    /**
    * @return the height of the object
    */
    public double getHeight(){
        return this.height;
    }

}
