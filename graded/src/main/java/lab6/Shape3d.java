package lab6;
public interface Shape3d{
    public double getVolume();
    public double getSurfaceArea();
}